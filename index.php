<?php
// web/index.php

require_once __DIR__.'/vendor/autoload.php';
require_once __DIR__.'/CaptchaProvider.php';
require_once __DIR__.'/Random.php';

$app = new Silex\Application(); 

$app->get('/v4/api/captcha', function() use($app) { 

    $captchaProvider = new CaptchaProvider();
    $random = new Random();
    $captchaProvider->setRandom($random);
    
    return $captchaProvider->getCaptcha()->toString(); 
}); 

$app->run();

?>