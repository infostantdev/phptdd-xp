<?php 
    require_once(dirname(__FILE__) . '/../CaptchaProvider.php');
    require_once(dirname(__FILE__) . '/../Random.php');
    class TestCaptchaProvider extends PHPUnit_Framework_TestCase {
        
        function testCaptcha() {
            $stub = $this->getMock('Random');
            $stub->expects($this->any())->method('next')->will($this->returnValue(2));
            
            $captchaProvider = new CaptchaProvider();
            $captchaProvider->setRandom($stub);
            $captcha = $captchaProvider->getCaptcha();

            $this->assertEquals("2 * Two", $captcha->toString());
        }


    }

