<?php
require_once(dirname(__FILE__) . '/../Captcha.php');

class TestCaptcha extends PHPUnit_Framework_TestCase {

    private $captcha;
    function setUp(){
        $this->captcha = new Captcha(1, 1, 1, 1);
    }
    function testLeftOperand(){
        $this->assertEquals("One", $this->captcha->getLeftOperand());
    }

    function testOperation(){

        $this->assertEquals("+", $this->captcha->getOperation());
    }

    function testRightOperand(){
        $this->assertEquals("1", $this->captcha->getRightOperand());
    }

    function testGetResult(){
        $this->assertEquals(2, $this->captcha->getResult());
    }

    function testCaseRightOperandWhenInputIs3WithFirstPattern(){
        $myCaptcha = new Captcha(1, 2, 1, 3);
        $this->assertEquals("3", $myCaptcha->getRightOperand());
    }

    function testCaseOperationWhenInputIs2WithFirstPattern(){
        $myCaptcha = new Captcha(1, 2, 2, 3);
        $this->assertEquals("*", $myCaptcha->getOperation());
    }

    function testCaseLeftOperandWhenInputIs2WithFirstPattern(){
        $myCaptcha = new Captcha(1, 2, 1, 1);
        $this->assertEquals("Two", $myCaptcha->getLeftOperand());
    }

    function testGetResultCaseWhenInput1211(){
        $myCaptcha = new Captcha(1, 2, 1, 1);
        $this->assertEquals(3, $myCaptcha->getResult());
    }
    
    function testGetResultCaseWhenInput1121(){
        $myCaptcha = new Captcha(1, 1, 2, 1);
        $this->assertEquals(1, $myCaptcha->getResult());
    }

    function testGetResultCaseWhenInput1931(){
        $myCaptcha = new Captcha(1, 9, 3, 1);
        $this->assertEquals(8, $myCaptcha->getResult());
    }

    function testGetResultCaseWhenInput2111(){
        $myCaptcha = new Captcha(2, 1, 1, 1);
        $this->assertEquals(2, $myCaptcha->getResult());
    }
    
    function testGetResultCaseWhenInput2121(){
        $myCaptcha = new Captcha(2, 1, 2, 1);
        $this->assertEquals(1, $myCaptcha->getResult());
    }

    function testGetResultCaseWhenInput2131(){
        $myCaptcha = new Captcha(2, 1, 3, 1);
        $this->assertEquals(0, $myCaptcha->getResult());
    }

    function testCaseLeftOperandWhenInputIs2WithSecondPattern(){
        $myCaptcha = new Captcha(2, 2, 1, 1);
        $this->assertEquals("2", $myCaptcha->getLeftOperand());
    }

    function testCaseRightOperandWhenInputIs3WithSecondPattern(){
        $myCaptcha = new Captcha(2, 2, 1, 3);
        $this->assertEquals("Three", $myCaptcha->getRightOperand());
    }

    function testLeftGreaterThanRight() {
        $myCaptcha = new Captcha(2, 3, 3, 4);
        $this->assertEquals('Invalid Range Exception', $myCaptcha->getResult());
    }

    function testIsNumeric() {
        
        
    }

    /**
     * @expectedException        InvalidArgumentException
     * @expectedExceptionMessage Invalid Range Exception
     */
    public function testExceptionHasRightMessage()
    {
        $myCaptcha = new Captcha('a', 'a', 'a', 'a');
        $myCaptcha->getResult();
    }
}
?>